var Nes = require('nes');
var fs = require('fs');
var prompt = require('prompt-sync')();

var connectSettings;
var ws_options = {}

try {
    connectSettings = JSON.parse(fs.readFileSync('robotSettings.json', 'utf8'));
    connectToServer(connectSettings.credentials, connectSettings.uuid);
} catch (err) {
    if (err.code === 'ENOENT') {
        var credentials = {};
        credentials.email = prompt('Enter email: ');
        credentials.password = prompt.hide('Enter password');
        connectToServer(credentials, null)
    } else {
        throw err
    }
}

function connectToServer(credentials, uuid) {
    var client = new Nes.Client('ws://localhost:8081');
    var buffer = new Buffer(credentials.email + ':' + credentials.password);
    var basicAuthCredentials = 'Basic ' + buffer.toString('base64');

    // client.connect({ auth: { headers: { authorization: basicAuthCredentials } } }, function (err) {
    client.connect(function (err) {
        if (err) {
            console.log(err.message);
            return;
        }
        client.request('/', function (err, payload) {
            if (err) {
                console.log(err.message)
            } else {
                var messageType = 'list'
                if (!uuid) {
                    messageType = 'register'
                }

                client.message({ type: messageType, credentials: credentials }, function (err, message) {
                    if (message.register) {
                        fs.writeFile('robotSettings.json', JSON.stringify({ credentials: credentials, uuid: message.robot.uuid }))
                    }
                    console.log(message);
                })
                console.log(payload)
            }
        })
    })
}

