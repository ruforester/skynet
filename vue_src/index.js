var Vue = require('vue');
var Home = require('./components/Home.vue');
var SignUp = require('./components/SignUp.vue');
var Login = require('./components/Login.vue');
var Logout = require('./components/Logout.vue');
var Robots = require('./components/Robots.vue');
var VueRouter = require('vue-router');
var VeeValidate = require('vee-validate');

Vue.use(VueRouter);
Vue.use(VeeValidate);

var routes = [
    { path: '/', component: Home },
    { path: '/signup', component: SignUp },
    { path: '/login', component: Login },
    { path: '/logout', component: Logout },
    { path: '/robots', component: Robots }
]

var router = new VueRouter({
    routes: routes
})

new Vue({
    el: '#app',
    router: router
})