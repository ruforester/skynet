const Hapi = require('hapi');
const Datastore = require('nedb');
const Bcrypt = require('bcrypt-nodejs');
const Boom = require('Boom');
const Joi = require('joi');
const AsyncParallel = require('async/parallel');
const Uuid = require('uuid');

const server = new Hapi.Server();
const http_server = server.connection({ port: 8080 });
const ws_server = server.connection({ port: 8081 });

const db = {};
db.users = new Datastore({ filename: './data/users.db', autoload: true });
db.robots = new Datastore({ filename: './data/robots.db', autoload: true });

db.users.find({ email: 'admin@sky.net' }, function (err, docs) {
    if (!docs.length) {
        let user = {};
        user.email = 'admin@sky.net';
        user.username = 'admin';
        hashPassword('aaazzz', function (err, hash) {
            if (err) {
                throw err
            }
            user.password = hash;
            db.users.insert(user, function (err, newDoc) {
                if (err) {
                    throw err;
                }
                console.log(newDoc);
            })
        })
    }
})

http_server.register(require('inert'), function (err) {
    if (err) {
        throw err;
    }
})

http_server.register(require('hapi-auth-cookie'), function (err) {
    if (err) {
        throw err
    }
    http_server.auth.strategy('cookie', 'cookie', {
        password: 'cookieLongStrongPasswordMustBe32CharacterLong',
        isSecure: false
    })
    http_server.auth.default('cookie')
})

const authUserSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{6,30}$/).required()
})

ws_server.register({
    register: require('nes'),
    options: {
        onConnection: function (socket) {
            console.log('Socket with id ' + socket.id + ' connected.');
        },
        onDisconnection: function (socket) {
            console.log('Socket with id ' + socket.id + ' disconnected.');

        },
        onMessage: function (socket, message, next) {
            switch (message.type) {
                case 'register':
                    var newRobot = {};
                    newRobot.email = message.credentials.email;
                    newRobot.uuid = Uuid();
                    db.robots.insert(newRobot, function (err, newDoc) {
                        if (err) {
                            next({ robot: newRobot, register: false });
                        } else {
                            next({ robot: newRobot, register: true });
                        }
                    })
                    break;
                case 'online':
                    next({ text: socket.auth })
                    break;
                case 'list':
                    listRobots(socket, function (roboList) {
                        next({ robots: roboList })
                    })
                    break;
                default:
                    break;
            }
        },
        auth: {
            type: 'cookie',
            isSecure: false,
            password: 'cookieLongStrongPasswordMustBe32CharacterLong'
        }
    }
}, function (err) {
    if (err) {
        throw err
    }

})

function listRobots(socket, cb) {
    db.robots.find({ email: socket.auth.credentials.email }, function (err, docs) {
        if (docs.length) {
            var _roboList = docs.map(function (obj) {
                var roboListItem = {};
                roboListItem.email = obj.email;
                roboListItem.uuid = obj.uuid;
                return roboListItem;
            });
            cb( _roboList);
        } else {
            cb([]);
        }
    })
}

const verifyCredentialsBasic = function (request, email, password, cb) {
    Joi.validate({ email: email, password: password }, authUserSchema, function (err, value) {
        if (err) {
            return cb(null, false)
        }
        db.users.find({ email: email }, function (err, docs) {
            if (docs.length) {
                Bcrypt.compare(password, docs[0].password, function (err, isValid) {
                    cb(err, isValid, { email: docs[0].email })
                })
            } else {
                return cb(null, false)
            }
        })
    })
}

ws_server.register(require('hapi-auth-cookie'), function (err) {
    if (err) {
        throw err;
    }
    ws_server.auth.strategy('ws_cookie', 'cookie', { 
    password: 'cookieLongStrongPasswordMustBe32CharacterLong'    
     });
    ws_server.auth.default('ws_cookie');
})

const createUser = function (request, reply) {
    let user = {};
    user.email = request.payload.email;
    user.username = request.payload.username;
    hashPassword(request.payload.password, function (err, hash) {
        if (err) {
            throw Boom.badRequest(err)
        }
        user.password = hash;
        db.users.insert(user, function (err, newDoc) {
            if (err) {
                throw Boom.badRequest(err)
            }
            reply('User saved')
        })
    })
}

const login = function (request, reply) {
    request.cookieAuth.set({ user: 'user' });
    reply.redirect('/')
}

const logout = function (request, reply) {
    request.cookieAuth.clear();
    reply({message: 'Succesfully logged out'})
}

const createUserSchema = Joi.object({
    username: Joi.string().alphanum().min(5).max(30).required(),
    email: Joi.string().email().required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{6,30}$/).required()
})



const verifyUniqueUser = function (request, reply) {
    let reqEmail = request.payload.email;
    let reqUsername = request.payload.username;
    let errors = [];
    let message = '';

    AsyncParallel([
        function (cb) {
            db.users.findOne({ email: reqEmail }, function (err, doc) {
                if (doc) {
                    message += ' Email taken';
                    errors.push({ field: "email", msg: "Email taken" });
                    cb();
                } else {
                    cb()
                }
            })
        },
        function (cb) {
            db.users.findOne({ username: reqUsername }, function (err, doc) {
                if (doc) {
                    message += ' Username taken';
                    errors.push({ field: "username", msg: "Username taken" });
                    cb();
                } else {
                    cb();
                }
            });
        }
    ], function (err) {
        if (errors.length) {
            let boomErr = Boom.conflict(message, errors);
            boomErr.output.payload.errData = boomErr.data;
            return reply(boomErr);
        }
        reply.continue()
    })



}

const verifyCredentials = function (request, reply) {
    let email = request.payload.email;
    let password = request.payload.password;
    db.users.find({ email: request.payload.email }, function (err, docs) {
        if (docs.length) {
            Bcrypt.compare(password, docs[0].password, function (err, isValid) {
                if (isValid) {
                    return reply(docs[0].username)
                } else {
                    return reply(Boom.unauthorized('Incorrect email or password'))
                }
            })
        } else {
            return reply(Boom.unauthorized('Incorrect email or password'))
        }
    })
}

http_server.route([
    {
        path: '/{param*}',
        method: 'GET',
        handler: {
            directory: {
                path: 'public'
            }
        },
        config: {
            auth: false
        }
    },
    {
        path: '/api/users/new',
        method: 'POST',
        handler: createUser,
        config: {
            pre: [
                { method: verifyUniqueUser }
            ],
            auth: false,
            validate: {
                payload: createUserSchema
            }
        }
    },
    {
        path: '/api/users/login',
        method: 'POST',
        handler: login,
        config: {
            pre: [
                { method: verifyCredentials, assign: 'user' }
            ],
            auth: false,
            validate: {
                payload: authUserSchema
            }
        }
    },
    {
        path: '/api/users/logout',
        method: 'GET',
        handler: logout,
        config: {
            auth: false
        }
    }
])

ws_server.route([
    {
        path: '/',
        method: 'GET',
        handler: function (request, reply) {
            return reply(request.auth.credentials.email)
        }
    }
])

function hashPassword(password, cb) {
    Bcrypt.genSalt(10, function (err, salt) {
        Bcrypt.hash(password, salt, null, function (err, hash) {
            return cb(err, hash)
        })
    })
}



server.start(function (err) {
    if (err) {
        throw err;
    }
    server.connections.forEach(function (connection) {
        console.log(`Server running at: ${connection.info.uri}`);
    })
})